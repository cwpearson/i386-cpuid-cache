#include "i386cache.hpp"

#include <stdint.h>
#include <cstdio>
#include <cstring>

void i386_cpuid_caches () {
  for (int cacheID = 0; cacheID < 32; cacheID++) {
  
    // Variables to hold the contents of the 4 i386 legacy registers
    uint32_t eax, ebx, ecx, edx; 

    eax = 4; // get cache info
    ecx = cacheID; // cache id

    __asm__ (
        "cpuid" // call i386 cpuid instruction
        : "+a" (eax) // contains the cpuid command code, 4 for cache query
        , "=b" (ebx)
        , "+c" (ecx) // contains the cache id
        , "=d" (edx)
    ); // generates output in 4 registers eax, ebx, ecx and edx 

    // EAX[4:0]
    unsigned cache_type = eax & 0x1F; 

    if (cache_type == 0) break; // end of valid cache identifiers

    char cache_type_string[32];
    switch (cache_type) {
        case 1:  strcpy(cache_type_string, "Data Cache\0");         break;
        case 2:  strcpy(cache_type_string, "Instruction Cache\0");  break;
        case 3:  strcpy(cache_type_string, "Unified Cache\0");      break;
        default: strcpy(cache_type_string, "Unknown Type Cache\0"); break;
    }

    // EAX[7:5]
    unsigned cache_level = (eax >> 5) & 0x7;

    // EAX[8]
    int cache_is_self_initializing = (eax >> 8) & 0x1; // does not need SW initialization
    // EAX[9]
    int cache_is_fully_associative = (eax >> 9) & 0x1;

    // ebx contains 3 integers of 10, 10 and 12 bits respectively
    unsigned cache_sets = ecx + 1;
    unsigned cache_coherency_line_size = (ebx & 0xFFF) + 1;
    unsigned cache_physical_line_partitions = ((ebx >> 12) & 0x3FF) + 1;
    unsigned cache_ways_of_associativity = ((ebx >> 22) & 0x3FF) + 1;

    // Total cache size is the product
    size_t cache_total_size = cache_ways_of_associativity * 
                              cache_physical_line_partitions * 
                              cache_coherency_line_size * 
                              cache_sets;

    printf(
        "Cache ID %d:\n"
        "- Level: %d\n"
        "- Type: %s\n"
        "- Sets: %d\n"
        "- System Coherency Line Size: %d bytes\n"
        "- Physical Line partitions: %d\n"
        "- Ways of associativity: %d\n"
        "- Total Size: %lu bytes (%lu kb)\n"
        "- Is fully associative: %s\n"
        "- Is Self Initializing: %s\n"
        "\n"
        , cacheID
        , cache_level
        , cache_type_string
        , cache_sets
        , cache_coherency_line_size
        , cache_physical_line_partitions
        , cache_ways_of_associativity
        , cache_total_size, cache_total_size >> 10
        , cache_is_fully_associative ? "true" : "false"
        , cache_is_self_initializing ? "true" : "false"
    );
  }
}
